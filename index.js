/**
 * Simpler js router.
 *
 * @see <a href="https://medium.freecodecamp.com/you-might-not-need-react-router-38673620f3d#.ot609zz0c">You might not need react router</a>
 */
import { pathToRegexp } from 'path-to-regexp';

/**
 * Given a list of available routes and information about the current request, find the correct route
 * and execute its handlers.
 *
 * @param {Array} routes A list of route objects with path, action, meta, etc. properties
 * @param {Object} context Information about the current request in pathname, error, etc. properties
 * @param {String} source Where is the request coming from (values: 'server', 'client', 'hydrate')
 * @param {*} stateOverride Optional: If we already know the state, we can pass it in here. This is
 *                          primarily useful when hydrating on the client side and the action method
 *                          needs the state to render matching markup to the server.
 *
 * @returns {Promise} A Promise that resolves to an array with the: element, meta, and state
 */
export default function resolve(routes, context, source, stateOverride) {

    // Init variables
    const uri = context.error ? '/error' : context.pathname;
    let keys = [];
    let match = null;

    // Attempt to find a matching route
    const route = routes.find((currentRoute) => {

        // Handle routes with multiple paths
        const paths = (typeof currentRoute.path === 'string') ? [currentRoute.path] : currentRoute.path;

        return paths.find((currentPath) => {

            // Reset keys after each search otherwise it can retain stale data
            keys = [];

            const pattern = pathToRegexp(currentPath, keys);
            match = pattern.exec(uri);
            return match;
        });
    });

    // If we couldn't find a route, try again for an error handler, then reject with a 404 error
    if (! route) {

        const error = new Error('Route not found');
        error.status = 404;

        if (context.error) { return Promise.reject(error); }

        return resolve(routes, { ...context, error }, source);
    }

    // Skip the first element (will be whole path)
    match.shift();

    // We found a matching route, get parameters
    const params = match.reduce((acc, matchItem, i) => {

        // Sanity check
        if (! matchItem) { return acc; }

        acc[keys[i].name] = matchItem;

        return acc;

    }, {});

    // Add the context and source to the params
    const data = { ...context, ...params, source };

    // A simple helper handle state for the route so we don't wind up with nested ternaries
    const getState = () => {

        // If we were given a state override, just use that
        if (stateOverride) return stateOverride;

        // If the route provides a state setter run it (on the server only)
        if (route.state && source === 'server') return route.state(data);

        // Ok, there's no state for this request
        return null;
    };

    return Promise.resolve(getState())
        .then((state) => Promise.all([
            Promise.resolve(route.action(data, state)),
            Promise.resolve(route.meta ? route.meta(data, state) : null),
            Promise.resolve(state)
        ]));
}
