/**
 * Test suite entrance point
 */
import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import resolve from '../index.js';

const expect = chai.expect;
chai.use(sinonChai);

const resolveArg = (arg) => Promise.resolve(arg);

const genRoute = (path) => ({
    path,
    action: sinon.spy(resolveArg),
    meta: sinon.spy(resolveArg),
    state: sinon.spy(resolveArg)
});

const routes = ['/test', '/error', ['/in/array', '/in/array/extra'], '/with/:param'].map(genRoute);
const routesNoError = ['/test'].map(genRoute);

routes.push({
    path: '/no/meta',
    action: sinon.spy(resolveArg),
    state: sinon.spy(resolveArg)
});

routes.push({
    path: '/no/state',
    action: sinon.spy(resolveArg),
    meta: sinon.spy(resolveArg)
});

describe('nih-router', function () {

    beforeEach(sinon.reset);

    it('should rock.', function () { return true; });

    it('should call the action function of the matched route', function () {

        return resolve(routes, { pathname: '/test' })
            .then(() => expect(routes[0].action).to.have.been.called);
    });

    it('should call the meta function of the matched route', function () {

        return resolve(routes, { pathname: '/test' })
            .then(() => expect(routes[0].meta).to.have.been.called);
    });

    it('should call the state function on the server', function () {

        return resolve(routes, { pathname: '/test' }, 'server')
            .then(() => expect(routes[0].state).to.have.been.called);
    });

    it('should still call action if no state function is defined', function () {

        return resolve(routes, { pathname: '/no/state' }, 'server')
            .then(() => expect(routes[5].action).to.have.been.called);
    });

    it('should not call the state function if not on the server', function () {

        return resolve(routes, { pathname: '/test' }, 'client')
            .then(() => expect(routes[0].state).to.not.have.been.called);
    });

    it('should match multiple paths in an array', function () {

        return resolve(routes, { pathname: '/in/array/extra' })
            .then(() => expect(routes[2].action).to.have.been.called);
    });

    it('should call the state function before calling action', function () {

        return resolve(routes, { pathname: '/test' }, 'server')
            .then(() => expect(routes[0].state.calledBefore(routes[0].action)).to.be.true);
    });

    it('should resolve meta to null if no meta function is defined', function () {

        return resolve(routes, { pathname: '/no/meta' })
            .then((results) => expect(results[1]).to.be.null);
    });

    it('should match the /error route if the no other route can be found', function () {

        return resolve(routes, { pathname: '/does/not/exist' })
            .then(() => expect(routes[1].action).to.have.been.called);
    });

    it('should reject with a 404 error if no match and no /error route', function () {

        return resolve(routesNoError, { pathname: '/does/not/exist' })
            .then(() => { throw new Error('Route should not resolve'); })
            .catch((error) => {

                expect(error.status).to.equal(404);
            });
    });

    it('should pass URL params to the action function', function () {

        return resolve(routes, { pathname: '/with/value' })
            .then((results) => expect(results[0].param).to.equal('value'));
    });

    it('should pass the source to the action function', function () {

        return resolve(routes, { pathname: '/test' }, 'hydrate')
            .then((results) => expect(results[0].source).to.equal('hydrate'));
    });

    it('should use state override if one was provided on the server', function () {

        return resolve(routes, { pathname: '/test' }, 'server', 'STATE OVERRIDE')
            .then((results) => expect(results[2]).to.equal('STATE OVERRIDE'));
    });

    it('should use state override if one was provided on the client', function () {

        return resolve(routes, { pathname: '/test' }, 'client', 'STATE OVERRIDE')
            .then((results) => expect(results[2]).to.equal('STATE OVERRIDE'));
    });
});
